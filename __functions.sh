#!/bin/sh

delete_link_or_backup() {
    path=$1

    if [ -e "$path" ]; then
        if [ -L "$path" ]; then
            echo "Suppression de l'ancien lien symbolique $path"
            rm "$path"
        else
            echo "Warning : $path déjà existant. Sauvegarde en cours."
            mv "$path" "$path-backup-$(date '+%Y_%m_%d')"
        fi
    fi
}

get_absolute_directory() {
    file=$1
    cd "$(dirname "$file")" || "error"
    pwd
}

inside_vm() {
    if command -v systemd-detect-virt >/dev/null 2>&1; then
        virt_system=$(systemd-detect-virt --vm)
        if [ x"$virt_system" != x"none" ]; then
            echo "yes"
        else
            echo "no"
        fi
        return
    fi

    virt_vendor=$(grep -i '\(qemu\|kvm\|rhv\|vmware\|virtualbox\)' /sys/class/dmi/id/* 2>/dev/null)
    if [ -n "$virt_vendor" ]; then
        echo "yes"
        return
    fi

    echo "undef"
}
