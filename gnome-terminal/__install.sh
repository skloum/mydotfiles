#!/bin/sh

. "${0%/*}/../__functions.sh"

module_dir=$(get_absolute_directory "$0")

# Vérification de l'existance de la commande gnome-terminal
if ! command -v gnome-terminal >/dev/null 2>&1; then
    echo "Warning : commande gnome-terminal introuvable."
    exit 0
fi

echo "Chargement des profils"
dconf reset -f /org/gnome/terminal/legacy/profiles:/ && dconf load /org/gnome/terminal/legacy/profiles:/ < "$module_dir/gnome-terminal-profiles.dconf"
