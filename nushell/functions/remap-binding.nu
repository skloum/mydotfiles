# Remap keybinding
export def main [
  current: record
  new: record
]: nothing -> list {
  let $current_binding = $env.config.keybindings | where modifier == $current.modifier and keycode == $current.keycode
  if ($current_binding | is-not-empty) {
    [
      {
        name: $"remap_($current_binding | last | get name)"
        modifier: $new.modifier
        keycode: $new.keycode
        mode: ($current_binding | last | get mode)
        event: ($current_binding | last | get event)
      }
    ]
  }
}
