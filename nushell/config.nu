# Nushell Config File
#

# Chargement des fonctions supplémentaires
export const FUNCTIONS_DIR = ($nu.default-config-dir | path join functions)
use ($FUNCTIONS_DIR | path join "remap-binding.nu")

# Configuration globale
$env.config.show_banner = false
$env.config.cursor_shape.emacs = "block"
$env.config.highlight_resolved_externals = true

# Environnements virtuels de développement en fonction du répertoire
# https://direnv.net
$env.config.hooks.env_change.PWD = $env.config.hooks.env_change.PWD? | append { ||
  if (which direnv | is-not-empty) {
    direnv export json | from json | default {} | load-env
  }
}

# Raccourcis personnalisés
$env.config.keybindings ++= [
  # Accès à l'historique avec Ctrl-R (redéfinition du raccourci par défaut
  # pour pouvoir ensuite le remapper sur Alt-R)
  {
    name: history_menu
    modifier: control
    keycode: char_r
    mode: [emacs, vi_insert, vi_normal]
    event: {
      send: menu
      name: history_menu
    }
  }
  # Utilisation de C-k et C-j pour naviguer dans l'historique et dans les menus
  {
    name: previous
    modifier: control
    keycode: char_k
    mode: [emacs, vi_normal, vi_insert]
    event: {
      until: [
        { send: menuprevious }
        { send: up }
      ]
    }
  }
  {
    name: next
    modifier: control
    keycode: char_j
    mode: [emacs, vi_normal, vi_insert]
    event: {
      until: [
        { send: menunext }
        { send: down }
      ]
    }
  }
  # M-C-k pour couper jusqu'à la fin de la ligne (habituellement attribué à C-k)
  {
    name: cut_line_to_end
    modifier: control_alt
    keycode: char_k
    mode: emacs
    event: { edit: cuttoend }
  }
  # M-C-u pour couper depuis le début de la ligne (habituellement attribué à C-u)
  {
    name: cut_line_from_start
    modifier: control_alt
    keycode: char_u
    mode: emacs
    event: { edit: cutfromstart }
  }
  # C-u pour couper toute la ligne
  {
    name: cut_whole_line
    modifier: control
    keycode: char_u
    mode: emacs
    event: { edit: cutcurrentline }
  }
]

# Starship
use ~/.cache/starship/init.nu

# Carapace
source ~/.cache/carapace/init.nu

# Zoxide
source ~/.cache/zoxide/init.nu

# Atuin
source ~/.cache/atuin/init.nu

# Remap de C-r en M-r
$env.config.keybindings ++= remap-binding { modifier: control keycode: char_r } { modifier: alt keycode: char_r }
