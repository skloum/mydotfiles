# Nushell Environment Config File
#

# Options pour "less" permettant en particulier d'afficher la sortie de "help"
$env.LESS = '-FRX'

# Utilisation de starship pour afficher le prompt
# https://starship.rs
mkdir ~/.cache/starship
if (which starship | is-not-empty) { starship init nu } else { '' } | save -f ~/.cache/starship/init.nu

# Utilisation de carapace pour fournir une complétion avancée des commandes externes
# https://carapace.sh/
$env.CARAPACE_BRIDGES = 'zsh,fish,bash,inshellisense'
mkdir ~/.cache/carapace
if (which carapace | is-not-empty) { carapace _carapace nushell } else { '' } | save -f ~/.cache/carapace/init.nu

# Utilisation de zoxide pour le déplacement rapide entre répertoires
# https://github.com/ajeetdsouza/zoxide
mkdir ~/.cache/zoxide
if (which zoxide | is-not-empty) { zoxide init nushell } else { '' } | save -f ~/.cache/zoxide/init.nu

# Utilisation de atuin pour la recherche dans l'historique
# https://docs.atuin.sh/
$env.ATUIN_CONFIG_DIR = (if ($env.XDG_CONFIG_HOME? | is-not-empty) { $"($env.XDG_CONFIG_HOME)/atuin/nushell" } else { $"($env.HOME)/.config/atuin/nushell" })
mkdir ~/.cache/atuin/
if (which atuin | is-not-empty) { atuin init nu --disable-up-arrow } else { '' } | save -f ~/.cache/atuin/init.nu
