#!/bin/sh

. "${0%/*}/../__functions.sh"

monitor_file=~/.config/monitors.xml

module_dir=$(get_absolute_directory "$0")

# Vérification de l'existance de la commande gnome-shell
if ! command -v gnome-shell >/dev/null 2>&1; then
    echo "Warning : commande gnome-shell introuvable."
    exit 0
fi

echo "Caps Lock utlisé comme deuxième Ctrl + Ctrl droit utilisé comme touche compose + Désactivation des espaces insécables"
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:ctrl_modifier', 'compose:rctrl', 'nbsp:none']"

echo "Super-Space (plutôt que Alt-Space) pour ouvrir le menu de la fenêtre active"
gsettings set org.gnome.desktop.wm.keybindings activate-window-menu "['<Super>space']"

echo "Affichage de la date et de l'heure"
gsettings set org.gnome.desktop.interface clock-show-weekday true
gsettings set org.gnome.desktop.interface clock-show-seconds true
gsettings set org.gnome.desktop.calendar show-weekdate true

echo "Focus suivant la souris"
gsettings set org.gnome.desktop.wm.preferences focus-mode 'mouse'

echo "Bindings de type 'Emacs' pour les applications Gtk"
gsettings set org.gnome.desktop.interface gtk-key-theme 'Emacs'

if [ "$(inside_vm)" = "yes" ]; then
    echo "Désactivation des animations"
    gsettings set org.gnome.desktop.interface enable-animations false

    echo "Changement de résolution"
    xrandr -s 1600x900
    delete_link_or_backup "$monitor_file"
    echo "Création du lien symbolique $monitor_file"
    ln -sr "$module_dir/monitors.xml" "$monitor_file"
fi
