#!/bin/sh

. "${0%/*}/../__functions.sh"

xdg_file=~/.config/starship.toml

module_dir=$(get_absolute_directory "$0")

# Vérification de l'existance de la commande starship
if ! command -v starship >/dev/null 2>&1; then
    echo "Warning : commande starship introuvable."
fi

# Création du lien symbolique dans ~/.config
delete_link_or_backup "$xdg_file"
echo "Création du lien symbolique $xdg_file"
ln -sr "$module_dir/starship.toml" "$xdg_file"
