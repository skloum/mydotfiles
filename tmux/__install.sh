#!/bin/sh

. "${0%/*}/../__functions.sh"

module_dir=$(get_absolute_directory "$0")
module_name=$(basename "$module_dir")
xdg_dir=~/.config

# Vérification de l'existance de la commande tmux
if ! command -v tmux >/dev/null 2>&1; then
    echo "Commande tmux introuvable. On ne fait rien..."
    exit 0
fi

# Création du lien symbolique dans ~/.config
delete_link_or_backup "$xdg_dir/$module_name"
echo "Création du lien symbolique $xdg_dir/$module_name"
ln -sr "$module_dir" "$xdg_dir/$module_name"

# tmux ne détecte le répertoire ~/.config/tmux qu'à partir de la version 3.1
# Pour les versions plus anciennes, il faut donc créer un lien symbolique de
# ~/.tmux.conf vers ~/.config/tmux/.tmux.conf
tmux_version=$(tmux -V | tr -cd '[:digit:].' | cut -d' ' -f2 | awk -F '.' '{print $1 * 100 + $2}')
if [ "$tmux_version" -lt "301" ]; then

    delete_link_or_backup "$HOME/.tmux.conf"
    echo "Création du lien symbolique ~/.tmux.conf"
    ln -sr $xdg_dir/$module_name/"tmux.conf" "$HOME/.tmux.conf"
fi
