#!/bin/sh

# shellcheck source=./__tmux_functions.sh
. "${0%/*}/__tmux_functions.sh"

# Récupération des arguments
pane_pid=$1
pane_tty=$2
pane_current_command=$3
pane_current_path=$4
direction=$5

## Récupération et extraction des infos sur le panneau
tty_info=$(_pane_info "$pane_pid" "$pane_tty")

tty_info_without_pid=${tty_info#*:}
command=${tty_info_without_pid#*:}

if [ "$pane_current_command" = "ssh" ]; then
    ## Si c'est une connection SSH, on réexecute la commande
    # shellcheck disable=SC2046
    tmux split-window "$direction" -c "$pane_current_path" $(echo "$command" | sed -e 's/;/\\;/g')
else
    tmux split-window "$direction" -c "$pane_current_path"
fi
