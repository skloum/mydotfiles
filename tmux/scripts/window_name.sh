#!/bin/sh

# shellcheck source=./__tmux_functions.sh
. "${0%/*}/__tmux_functions.sh"

# Récupération des arguments
pane_pid=$1
pane_tty=$2
pane_current_command=$3
pane_current_path=$4

## Récupération et extraction des infos sur le panneau
tty_info=$(_pane_info "$pane_pid" "$pane_tty")

tty_info_without_pid=${tty_info#*:}
username=${tty_info_without_pid%:*}
command=${tty_info_without_pid#*:}

## Tests pour savoir si la commande courante correspond à un shell supporté
case $pane_current_command in
    "bash") pane_current_command_shell="🐚";;
    "fish") pane_current_command_shell="🐠";;
    "nu") pane_current_command_shell="ν";;
    *) pane_current_command_shell="unknown";;
esac

if [ "$username" = "root" ]; then
    ## Si c'est un shell root, c'est l'info qu'on affiche en priorité
    window_name="root:shell"
elif [ "$pane_current_command" = "ssh" ]; then
    ## Si c'est une connection SSH, on affiche le nom de l'hôte
    command_args=${command#* }
    # shellcheck disable=SC2086
    hostname=$(ssh -G $command_args 2>/dev/null | awk '/^hostname / { print $2; exit }')
    # Si ce n'est pas une adresse IP, on ne garde que le nom court
    if ! expr "$hostname" : '[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$' >/dev/null; then
        hostname=${hostname%%.*}
    fi
    window_name="ssh:$hostname"
elif [ "$pane_current_command_shell" != "unknown" ]; then
    ## On tente d'abord de savoir si c'est un shell interactif ou un script en cours d'exécution
    # Lorsque c'est le shell interactif par défaut la commande complète est le
    # nom du shell préfixé par "-"
    if [ "-$pane_current_command" = "$command" ]; then
        shell_interactif=1
    elif [ "$pane_current_command" = "$command" ]; then
        shell_interactif=1
        shell_icon=" [$pane_current_command_shell]"
    fi

    ## Si c'est un shell interactif, on affiche le projet git ou le chemin courant
    if [ "$shell_interactif" -eq 1 ]; then
        cd "$pane_current_path" || exit
        git_dir=$(dirname "$(git rev-parse --absolute-git-dir)")
        if [ "$git_dir" != "." ]; then
            if [ "$pane_current_path" = "$git_dir" ]; then
                git_name="$(basename "$pane_current_path")"
            else
                git_name="$(basename "$git_dir")➔$(basename "$pane_current_path")"
            fi
            window_name="git:$git_name$shell_icon"
        else
            # Suppression des blancs et remplacement du homedir par ~
            dir_name=$(echo "$pane_current_path" | sed "s/ /_/g"  | sed "s|^$HOME|~|")

            # Lorque le chemin est trop long, on ne garde que le dernier répertoire
            if [ ${#dir_name} -gt 25 ]; then
                dir_name="…/"$(basename "$dir_name")
            fi
            window_name="dir:$dir_name$shell_icon"
        fi
    else
        window_name="cmd:${command%% *}"
    fi
else
    ## Sinon on affiche la commande en cours
    window_name="cmd:$pane_current_command"
fi

printf '%s' "$window_name"
