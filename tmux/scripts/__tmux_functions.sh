#!/bin/sh

# Fonction copiée depuis oh-my-tmux : https://github.com/gpakosz/.tmux
# Retourne le pid, l'utilisateur et la commande en cours
# Adaptation : considère tous les processus sauf les zombies
_pane_info() {
    pane_pid="$1"
    pane_tty="${2##/dev/}"
    ps -t "$pane_tty" --sort=lstart -o user=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -o pid= -o ppid= -o state= -o command= | awk -v pane_pid="$pane_pid" '
            $4 != "Z" {
            user[$2] = $1; if (!child[$3]) child[$3] = $2; pid=$2; $1 = $2 = $3 = $4 = ""; command[pid] = substr($0,5)
            }
            END {
              pid = pane_pid
              while (child[pid])
                pid = child[pid]

              print pid":"user[pid]":"command[pid]
            }
          '
}
