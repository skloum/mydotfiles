#!/bin/sh

# Récupération des arguments
window_index=$1
pane_pid=$2
pane_tty=$3
pane_current_command=$4
pane_current_path=$5
automatic_rename=$6
static_window_name=$7
current=$8

# Chargement des variables tmux
eval "$(tmux show-environment -gh | sed -e 's/^/export /')"

if [ "$automatic_rename" = "0" ]; then
    color="${_renamed_window_color:?}"
    name="${_renamed_window_symbol:?} $static_window_name"
else
    window_name=$("${0%/*}/window_name.sh" "$pane_pid" "$pane_tty" "$pane_current_command" "$pane_current_path")
    mode=${window_name%:*}
    name=${window_name#*:}

    color=""
    eval color="\$_${mode}_window_color"
    symbol=""
    eval symbol="\$_${mode}_window_symbol"
    symbol="$symbol "

    # Cas particulier du shell root : nom forcé à root
    if [ "$mode" = "root" ]; then
        name="root"
    fi
fi

# Ajout d'un espace pour laisser un vide entre le statut de gauche et la première fenêtre
if [ "$window_index" -eq 1 ]; then
    printf ' '
fi

## Inversion des couleurs pour la fenêtre courante
if [ "$current" = "true" ]; then
    printf '%s' "#[fg=$color]${_left_separator:?}#[bg=$color,fg=${_status_bgcolor:?},bold]$window_index:$symbol$name#[default]#[fg=$color]${_right_separator:?}#[default]"
else
    printf '%s' "#[default]#[fg=$color,bold] $window_index:$symbol$name #[default]"
fi
