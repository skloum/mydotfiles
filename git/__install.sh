#!/bin/sh

. "${0%/*}/../__functions.sh"

module_dir=$(get_absolute_directory "$0")
module_name=$(basename "$module_dir")
xdg_dir=~/.config

# Création du lien symbolique dans ~/.config
delete_link_or_backup "$xdg_dir/$module_name"
echo "Création du lien symbolique $xdg_dir/$module_name"
ln -sr "$module_dir" "$xdg_dir/$module_name"
