# Pas de chargement de la configuration décrite dans ce fichier dans les cas suivants

# si la variable d'environment TERM a pour valeur dumb (usage de TRAMP depuis Emacs)
test "$TERM" = "dumb"; and exit 0

# si l'utilisation est non interactive
status is-interactive; or exit 0


####
#  _____            _                                                 _
# | ____|_ ____   _(_)_ __ ___  _ __  _ __   ___ _ __ ___   ___ _ __ | |_
# |  _| | '_ \ \ / / | '__/ _ \| '_ \| '_ \ / _ \ '_ ` _ \ / _ \ '_ \| __|
# | |___| | | \ V /| | | | (_) | | | | | | |  __/ | | | | |  __/ | | | |_
# |_____|_| |_|\_/ |_|_|  \___/|_| |_|_| |_|\___|_| |_| |_|\___|_| |_|\__|
#
####

# Gestion du path
fish_add_path -mP ~/bin ~/.local/bin ~/.cargo/bin

# Editeur par défaut
if command -q emacsclient
    set -gx EDITOR "emacsclient -t" # Editeur par défaut en mode terminal
    set -gx VISUAL "emacsclient -c" # Editeur par défaut en mode graphique
    set -gx ALTERNATE_EDITOR "vim"  # Si pas de daemon Emacs, on utilise vim
else
    set -gx EDITOR "vim"
    set -gx VISUAL "vim"
end

# FZF
set -gx FZF_DEFAULT_OPTS '--cycle --layout=reverse --border --height 90% --preview-window=wrap'
set -gx FZF_DEFAULT_COMMAND 'fdfind --type f --hidden --exclude .git'
if __inside_tmux
    alias fzf "fzf-tmux -d 90%"
end

# Pas de message d'accueil
set -gx fish_greeting ''


####
#     _    _ _
#    / \  | (_) __ _ ___  ___  ___
#   / _ \ | | |/ _` / __|/ _ \/ __|
#  / ___ \| | | (_| \__ \  __/\__ \
# /_/   \_\_|_|\__,_|___/\___||___/
#
###
if command -q batcat
    set batcmd batcat
else if command -q bat
    set batcmd bat
end
if set -q batcmd
    set -gx BAT_THEME OneHalfDark
    alias less "$batcmd --paging=always -p"
    function zless
        zcat $argv | $batcmd --paging=always -p
    end
end

alias ipinfo "curl ipinfo.io"
alias ipconfig "ip --color addr"
alias ipname "host (hostname)"
alias uproot "cd (dirname (git rev-parse --absolute-git-dir))"
alias tmp-ssh "ssh -o GlobalKnownHostsFile=/dev/null -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
alias old-ssh "TERM=xterm-256color ssh -o KexAlgorithms=+diffie-hellman-group1-sha1 -o HostKeyAlgorithms=+ssh-rsa -o PubkeyAcceptedAlgorithms=+ssh-rsa"
alias pwd-ssh "ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no"
alias debug-emacs "pkill -SIGUSR2 emacs"

# Les commandes podman et docker fournissent les mêmes options
if command -q podman && ! command -q docker
    alias docker "podman"
end

# Alias pour Doom Emacs si présent
if test -d ~/.config/doom-emacs
    alias doom-emacs "emacs --init-directory=~/.config/doom-emacs"
end

# Pour pouvoir faire "su -" en présevant les infos concernant l'éventuelle
# connexion SSH (utile pour l'affichage du prompt avec starship) et en étant
# certain d'exécuter un shell fish
alias su "su --shell=$__fish_bin_dir/fish --whitelist-environment=SSH_CLIENT,SSH_CONNECTION,SSH_TTY"

# Pour l'intéret des abbréviations : https://www.sean.sh/log/when-an-alias-should-actually-be-an-abbr/
abbr -a ect emacsclient -t
abbr -a ecg emacsclient -c


####
#  ____                            _
# |  _ \ _ __ ___  _ __ ___  _ __ | |_
# | |_) | '__/ _ \| '_ ` _ \| '_ \| __|
# |  __/| | | (_) | | | | | | |_) | |_
# |_|   |_|  \___/|_| |_| |_| .__/ \__|
#                           |_|
####

# Utilisation de starship pour la partie gauche du prompt
# https://github.com/starship/starship
if command -q starship
    starship init fish | source
end


####
#  _  __          _     _           _ _
# | |/ /___ _   _| |__ (_)_ __   __| (_)_ __   __ _ ___
# | ' // _ \ | | | '_ \| | '_ \ / _` | | '_ \ / _` / __|
# | . \  __/ |_| | |_) | | | | | (_| | | | | | (_| \__ \
# |_|\_\___|\__, |_.__/|_|_| |_|\__,_|_|_| |_|\__, |___/
#            |___/                             |___/
####

bind \cx\ce __edit_commandline  # C-x C-e
bind \cp __toggle_private_mode  # C-p
command -q fzf; and bind \e\cl __fzf_search_git_log # M-C-l

# Utilisation de C-k et C-j pour naviguer dans l'historique
bind \ck up-or-search
bind \cj down-or-search
# M-C-k pour couper jusqu'à la fin de la ligne (habituellement attribué à C-k)
bind \e\ck kill-line
# M-C-u pour couper depuis le début de la ligne (habituellement attribué à C-u)
bind \e\cu backward-kill-line
# C-u pour couper toute la ligne
bind \cu kill-whole-line


####
#  ____  _
# |  _ \(_)_   _____ _ __ ___
# | | | | \ \ / / _ \ '__/ __|
# | |_| | |\ V /  __/ |  \__ \
# |____/|_| \_/ \___|_|  |___/
#
####

# Navigation rapide entre répertoires
# https://github.com/ajeetdsouza/zoxide
if command -q zoxide
    set -gx _ZO_ECHO 1
    zoxide init fish | source
end

# Environnements virtuels de développement en fonction du répertoire
# https://direnv.net
if command -q direnv
    # Création du hook
    direnv hook fish | source

    # Mise en place de l'environnement dès le lancement du shell si nécessaire
    # Problème corrigé à partir de la version 2.26.0 de direnv
    # (cf https://github.com/direnv/direnv/issues/583)
    if test (vercmp (direnv --version) 2.26.0) -lt 0
        direnv export fish | source
    end
end

# Intégration avec Emacs/vterm
# https://github.com/akermu/emacs-libvterm#shell-side-configuration
if test vterm = "$INSIDE_EMACS" -a -f "$EMACS_VTERM_PATH/etc/emacs-vterm.fish"
    source "$EMACS_VTERM_PATH/etc/emacs-vterm.fish"

    function ff
        set -q argv[1]; or set argv[1] "."
        vterm_cmd find-file (realpath "$argv")
    end
end

# Recherche dans l'historique avec atuin (ou fzf)
# https://docs.atuin.sh/
if command -q atuin
    if set -q XDG_CONFIG_HOME
        set -gx ATUIN_CONFIG_DIR "$XDG_CONFIG_HOME/atuin/fish"
    else
        set -gx ATUIN_CONFIG_DIR "$HOME/.config/atuin/fish"
    end
    atuin init fish --disable-up-arrow | source
else if command -q fzf
    bind \cr __fzf_search_history # C-r
end

# Remap de C-r en M-r
__remap_binding '\cr' '\er'
