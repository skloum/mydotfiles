function __toggle_private_mode --description "Toggle private mode"
    if set -q fish_private_mode
        set -e fish_private_mode
    else
        set -gx fish_private_mode 1
    end
    commandline -f repaint
end
