function __edit_commandline --description "Use $EDITOR to edit the current command"
    set -q EDITOR; or return 1
    set -l tmpfile (mktemp).fish; or return 1
    commandline > $tmpfile
    eval $EDITOR $tmpfile
    commandline -r -- (cat $tmpfile)
    rm $tmpfile
end
