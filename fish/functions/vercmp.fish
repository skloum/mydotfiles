function vercmp --description "Compare versions. Prints: -1 if the first version is smaller ; 1 if the first version is greater ; 0 if both are equal"
    if not set -q argv[2]
        echo "Expected two arguments" >&2
        return 2
    end

    if test "$argv[1]" = "$argv[2]"
        echo 0
        return 0
    end

    if echo $argv[1]\n$argv[2] | sort --check=silent --version-sort
        echo -1
        return 0
    else
        echo 1
        return 0
    end
end
