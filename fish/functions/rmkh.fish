function rmkh -d "Removes a given host from ~/.ssh/known_hosts"
    ssh-keygen -R "$argv[1]"

    set ip (dig +short $argv[1])
    if test -n "$ip"
        ssh-keygen -R "$ip"
    end
end
