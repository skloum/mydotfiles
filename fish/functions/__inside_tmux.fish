function __inside_tmux -d "Test if we are inside a tmux session"
    test (ps -p (string trim (ps -o ppid= -p $fish_pid)) -o comm=) = "tmux: server"
end
