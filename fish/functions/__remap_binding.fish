function __remap_binding --description "Remap keybinding"
    set current $argv[1]
    set new $argv[2]

    set current_function (bind -k | grep $current | tail -1 | rev | cut -d" " -f1 | rev)
    test -z $current_function; or bind (string unescape $new) $current_function
end
