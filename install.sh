#!/bin/sh

dotfiles_dir=${0%/*}

for entry in "$dotfiles_dir"/*
do
    if [ -d "$entry" ] && [ -x "$entry"/__install.sh ]; then
        software=$(basename "$entry")
        while true; do
            printf "* Installation de la configuration pour %s ? (y/n) " "$software"
            read -r answer
            case $answer in
                [Yy] ) "$entry"/__install.sh; break;;
                [Nn] ) break;;
                * ) echo "Réponse : y ou n";;
            esac
        done
    fi
done
