#!/bin/sh

. "${0%/*}/../__functions.sh"

module_dir=$(get_absolute_directory "$0")

delete_link_or_backup "$HOME/.vimrc"
echo "Création du lien symbolique ~/.vimrc"
ln -sr "$module_dir/vimrc" "$HOME/.vimrc"
